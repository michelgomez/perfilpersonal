function translatePage() {
    var apiKey = 'TU_API_KEY'; 
    var sourceLang = 'es'; 
    var targetLang = 'en'; 

    var titleElement = document.getElementById('title');
    var contentElement = document.getElementsByClassName('content');

    var titleText = titleElement.textContent;
    var contentText = contentElement.textContent;

    // Llamada a la API de traducción de Google
    $.ajax({
        url: 'https://translation.googleapis.com/language/translate/v2?key=' + apiKey,
        type: 'POST',
        data: {
            q: [titleText, contentText],
            source: sourceLang,
            target: targetLang
        },
        success: function(response) {
            var translations = response.data.translations;
            var translatedTitle = translations[0].translatedText;
            var translatedContent = translations[1].translatedText;

            titleElement.textContent = translatedTitle;
            contentElement.textContent = translatedContent;
        },
        error: function(error) {
            console.log(error);
        }
    });
}
